package encoders;

// omitted imports

/**
 * The process of adding PID control to any CTRE device is very similar, regardless of Control Mode
 * and specific device type
 */
public class VelocityClosedLoopTalonSRX extends SubsystemBase {

    private TalonSRX talonsrx;

    public VelocityClosedLoopTalonSRX() {

        talonsrx = new TalonSRX(0);


        talonsrx.configFactoryDefault();

        /* Ensure sensor is positive when output is positive, read javadocs for more info */
		talonsrx.setSensorPhase(Constants.kSensorPhase);

        /**
		 * Set based on what direction you want forward/positive to be.
		 * This does not affect sensor phase. 
		 */ 
		talonsrx.setInverted(Constants.kMotorInvert);


        /**
         * Relative: The position of "0" can change, either by setting it or changing it when the robot is powered off
         * Absolute: The position of "0" will never change
         * 
         * Velocity closed loops should be relative
         */
        talonsrx.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 30); // arbitrary values can be changed later if necessary

        /**
		 * Config the allowable closed-loop error, Closed-Loop output will be
		 * neutral within this range.
		 */
		talonsrx.configAllowableClosedloopError(0);

		/* Config Velocity Closed Loop gains. */
		talonsrx.config_kF(0, Constants.kGains.kF);
		talonsrx.config_kP(0, Constants.kGains.kP);
		talonsrx.config_kI(0, Constants.kGains.kI);
		talonsrx.config_kD(0, Constants.kGains.kD);
    }

    /**
     * CTRE measures velocity in units per tenth second, rather than RPM
     * 
     * These calculations may not be correct, users are encouraged to verify
     * 
     * @param velocity Velocity in RPM
     */
    public void setVelocity(double velocity) {

        // Conversion to CTRE Native units

        final double UNITS_PER_ROT = 4096; // encoder units per rotation
        final double UNITS_PER_MINUTE = UNITS_PER_ROT * velocity; // convert from revolutions per minute to encoder units per minute
        final double UNITS_PER_TENTH_SECOND = UNITS_PER_MINUTE / (60 * 10); // 60 seconds per minute, 10 deciseconds per second

        talonsrx.set(ControlMode.Velocity, UNITS_PER_TENTH_SECOND);
    }  
}