package encoders;

// omitted imports


/**
 * The process of adding PID control to any CTRE device is very similar, regardless of Control Mode
 * and specific device type
 */
public class PositionClosedLoopTalonFX extends SubsystemBase {

    private TalonFX talonfx;

    public PositionClosedLoopTalonFX() {

        talonfx = new TalonFX(0);


        talonfx.configFactoryDefault();

        /* Ensure sensor is positive when output is positive, read javadocs for more info */
		talonfx.setSensorPhase(Constants.kSensorPhase);

        /**
		 * Set based on what direction you want forward/positive to be.
		 * This does not affect sensor phase. 
		 */ 
		talonfx.setInverted(Constants.kMotorInvert);

        talonfx.configSelectedFeedbackSensor(TalonFXFeedbackDevice.IntegratedSensor, 0, 30); // arbitrary values can be changed later if necessary

        /**
		 * Config the allowable closed-loop error, Closed-Loop output will be
		 * neutral within this range.
		 */
		talonfx.configAllowableClosedloopError(0);

		/* Config Position Closed Loop gains, typically kF stays zero with a position loop. */
		talonfx.config_kF(0, Constants.kGains.kF);
		talonfx.config_kP(0, Constants.kGains.kP);
		talonfx.config_kI(0, Constants.kGains.kI);
		talonfx.config_kD(0, Constants.kGains.kD);
    }

    public void setPosition(double angle) {
        /**
         * 360 degrees = 4096 encoder units, therefore:
         * (4096*angle)/360 = angle in encoder units
         */
        talonfx.set(ControlMode.Position, (4096*angle)/360);
    }
}