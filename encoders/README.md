# Encoders

There are multiple types of CTRE Encoders:

- CanCoder
- SRX Mag Encoder
- Builtin Falcon Encoder
- The NEO 550 motor (along with a SparkMAX motor controller) also has a built-in encoder, mainly used for velocity tracking

All of these require the [CTRE Vendor Library](https://maven.ctr-electronics.com/release/com/ctre/phoenix/Phoenix-frc2022-latest.json), which can be installed with <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>p</kbd>, and selecting `WPILib: Manage Vendor Libraries` -> `Install new libraries (online)`, and pasting the link.

The SparkMAX requires the [REV Vendor Library](https://software-metadata.revrobotics.com/REVLib.json), which can be installed in the same way

All of the CTRE Encoders and control methods (Position, Velocity, Motion Magic, etc.) have very similar usages.