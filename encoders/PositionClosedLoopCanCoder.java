package encoders;

// omitted imports


/**
 * CANCoders can either act as a standalone sensor, or as a closed loop input for another CTRE motor controller
 * This example uses it as a PID Control input for a TalonFX
 */
public class PositionClosedLoopCanCoder {
    
    private TalonFX talonfx;
    private CANCoder canCoder;

    public PositionClosedLoopCanCoder() {
        talonfx = new TalonFX(Constants.TALON_FX_PORT);
        canCoder = new TalonFX(Constants.CANCODER_PORT);

        TalonFXConfiguration talonconfig = new TalonFXConfiguration();

        // Configure PID constants
        // kF can be included for a velocity closed loop
        talonconfig.slot0.kP = 0;
        talonconfig.slot0.kI = 0;
        talonconfig.slot0.kD = 0;

        // Configure CanCoder as a feedback device for the TalonFX
        talonconfig.remoteFilter0.remoteSensorDeviceID = canCoder.getDeviceID();
        talonconfig.remoteFilter0.remoteSensorSource = RemoteSensorSource.CANCoder;
        talonconfig.primaryPID.selectedFeedbackSensor = FeedbackDevice.RemoteSensor0;
        talonfx.configAllSettings(talonconfig);

    }

    public void setPosition(double angle) {
        /**
         * 360 degrees = 4096 encoder units, therefore:
         * (4096*angle)/360 = angle in encoder units
         */
        talonfx.set(ControlMode.Position, (4096*angle)/360);
    }
    
}
