package encoders;

// omitted imports

public class VelocityClosedLoopSparkMax {

    private CANSparkMax sparkmax;
    private CANPIDController controller;

    public VelocityClosedLoopSparkMax() {

        sparkmax = new CANSparkMax(Constants.SPARK_MAX_PORT, MotorType.kBrushless);

        sparkmax.restoreFactoryDefaults();

        // REV uses a seperate PID controller object
        // The Encoder is built into the Neo 550 motor and is connected directly to the SparkMAX
        controller = sparkmax.getPIDController();


        /**
         * Set PIDF Constants
         * Currently contains arbitrary values
         */
        controller.setP(0.0001);
        controller.setI(0);
        controller.setD(0);
        controller.setIZone(100);
        controller.setFF(.000095);

    }

    /**
     * Sets the PIDF setpoint of the small flywheel
     * @param speed The rotational velocity in RPM
     */
    public void setSpeed(double speed) {
        // REV uses RPM natively
        controller.setReference(speed, ControlType.kVelocity);
    }

    
}
